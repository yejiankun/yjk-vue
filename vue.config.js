// module.exports = {
//   baseUrl: process.env.NODE_ENV === "production" ? "/FEIGN" : "/",
//   devServer: {
//     port: 8089,
//     proxy: "http://localhost:5001"
//   },
//   chainWebpack: config => {
//     config.merge({
//       externals: {
//         "CKEDITOR": "window.CKEDITOR"
//       }
//     });
//   }
// };


module.exports = {
  proxy: {
    '/FEIGN': {    //将www.exaple.com印射为/apis
      target: 'http://localhost:5001',  // 接口域名
      secure: false,  // 如果是https接口，需要配置这个参数
      changeOrigin: true,  //是否跨域
      pathRewrite: {
        '^/apis': ''   //需要rewrite的,
      }
    }
  }
}
